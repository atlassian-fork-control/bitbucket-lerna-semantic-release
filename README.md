# Bitbucket lerna semantic release

## Develop

The easiest way to get started modifying this package is to create a lerna repository (must contain a `lerna.json` file) on Bitbucket (or fork an existing one).

Next, run:

```
npm run dev
```

This will run a webpack dev server on `localhost:8080`. You will now need to use [ngrok](https://ngrok.com/) to expose that local server to the internet.

```
ngrok http 8080
```

This should give you an ngrok address (should look like "https://7fba2abe.ngrok.io") that you can test by going to "https://7fba2abe.ngrok.io/dist/atlassian-connect.json". You should see the json file getting served.

You'll need to modify the `dist/atlassian-connect.json` file's baseUrl field to be this url (without the '/dist/...' on the end).

Now go to your Bitbucket `Manage Integrations` screen (https://bitbucket.org/account/user/[YOUR-USERNAME]/addon-management) and click `Install add-on from URL`. Paste your atlassian-connect.json url from above in here (with the `/dist/atlassian-connect.json` at the end).

Now simply open a PR in your repo and you should your addon running. Any changes you make locally should be reflected if you hard refresh your browser.

## Deployment

This addon is served from [firebase](https://firebase.google.com/). To release a change, first make a PR into the repo.

**Make sure you don't check in the `atlassian-connect.json` file with the baseUrl pointing to ngrok!**

**Set it to https://bb-lerna-semantic-release.firebaseapp.com**

Next you'll need the [firebase CLI tools](https://firebase.google.com/docs/cli/).

* `npm isntall -g firebase-tools`
* Login to Firebase `firebase login`
* `npm run deploy`
* Thats it!
