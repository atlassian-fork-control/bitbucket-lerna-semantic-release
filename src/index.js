import md from 'marked';

import queryString from 'query-string';
import getCommits from './get-commits';
import getPackages from './get-packages';
import getReleaseType from './get-release-type';
import getChangelog from './get-changelog';

function typeToEmoji(type) {
  if (type === 'patch') return '🛠';
  if (type === 'minor') return '✨';
  if (type === 'major') return '💥';
  return '';
}

function renderChangelog(releases) {
  return releases.map(({ pkg, changelog }) => (`
    <h2>${pkg}</h2>
    ${md(changelog)}
  `)).join('');
}
function renderReleaseTypes(releases) {
  const list = releases.map(({ pkg, type }) => `
    <li>
      ${pkg} (${typeToEmoji(type)} ${type})
    </li>
  `);

  return `<ul>
    ${list.join('')}
  </ul>`;
}


function render(releases) {
  const relevantReleases = releases.filter(({ type }) => !!type);
  if (!relevantReleases.length) {
    return '<div style="color: red; border: 1px solid; padding: 10px; border-radius: 10px; display: inline-block;">' +
     'No packages will be released with this PR <br />' +
     '<b>Ensure your commit messages have the correct affects line format</b>' +
     '</div>';
  }
  const releaseTypes = renderReleaseTypes(relevantReleases);
  const changelog = renderChangelog(relevantReleases);
  return `
    ${releaseTypes}
    <h1>📄 Changelog additions</h1>
    ${changelog}
  `;
}

const { user, repo, pullrequestid } = queryString.parse(window.location.search);
getCommits(user, repo, pullrequestid).then((commits) => {
  const packages = getPackages(commits);
  // eslint-disable-next-line prefer-arrow-callback
  const packagePromises = packages.map(pkg => (new Promise((resolve, reject) => {
    getReleaseType(commits, pkg).then((type) => {
      getChangelog(commits, pkg).then((changelog) => {
        resolve({
          pkg,
          type,
          changelog,
        });
      });
    }, error => reject(error));
  })));

  document.body.innerHTML = '';
  Promise.all(packagePromises).then((packageData) => {
    document.body.innerHTML = render(packageData);
  });
});
